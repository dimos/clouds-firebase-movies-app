import './App.css';
import { useNavigate } from "react-router-dom";
import {Navbar, Nav, Container, Button} from 'react-bootstrap'
import {
  BrowserRouter,
  Routes,
  Route,
  Link,
} from "react-router-dom";
import React, { useState } from "react";
import logo from './popcorn.png';
import heart from './heart.png';
import LoginPage from './LoginPage';
import HomePage from './HomePage';
import WishlistPage from './WishlistPage';

function App() {
  const [pageName, setPageName] = useState("Login")

  const loggedName = localStorage.getItem("name");
  if (loggedName != null && pageName == "Login") {
    setPageName("HomePage");
  }
  else if (loggedName == null && pageName != "Login") {
    setPageName("Login");
  }

  // console.log(pageName, localStorage.getItem("name"));
  if (pageName == "Login") {
    return (
      <LoginPage setPageName={setPageName}/>
    );
  }

  function handleLogout() {
    localStorage.clear();
    setPageName("Login");
  }

  return (
    <BrowserRouter>
    <div className="App">
      <>
        <Navbar bg="dark" variant="dark">
          <Container>
            <Navbar.Brand as={Link} to="/home">
              <img
                alt=""
                src={logo}
                width="30"
                height="30"
                className="d-inline-block align-top"
              />{' '}
            Movies Room
            </Navbar.Brand>
            <Nav className="mr-auto">
              <Navbar.Text bg="light">
                <a>{loggedName}</a>
              </Navbar.Text>
              <Nav.Link as={Link} to="/wishlist">
                <img
                  alt=""
                  src={heart}
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                />
              </Nav.Link>
              <Nav.Link as={Link} to="/home">
                <Button variant="secondary" size="lg" onClick={handleLogout}>
                      Logout
                </Button>
              </Nav.Link>
            </Nav>
          </Container>
        </Navbar>
      </>
      <div>
        <Routes>
          <Route path="/" element={<HomePage/>}/>
          <Route path="/home" element={<HomePage/>}/>
          <Route path="/wishlist" element={<WishlistPage/>}/>
        </Routes>
      </div>
    </div>
    </BrowserRouter>
  )
}

export default App;
