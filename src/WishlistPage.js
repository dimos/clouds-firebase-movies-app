import React, { useState, useRef } from "react";
import {Form, Table, Button} from 'react-bootstrap';

import { database } from './Firebase';
import { ref, child, get } from "firebase/database";
import MovieElement from './MovieElement';

export default function WishlistPage(props) {

  const [moviesList, setMoviesList] = useState([])
  const [wishList, setWishList] = useState([])
  const [searchBarVal, setSearchBarVal] = useState("")

  if (moviesList.length == 0) {
    get(child(ref(database), `movies-list`)).then((snapshot) => {
      if (snapshot.exists()) {
        setMoviesList(snapshot.val())
        // console.log(snapshot.val());
      } else {
        console.log("No data available");
      }
    }).catch((error) => {
      console.error(error);
    });
  }

  const loading = moviesList.length == 0;

  // console.log(wishList)
  const storedWishlistJSON = localStorage.getItem("wishlist")
  var storedWishlist
  if (storedWishlistJSON) {
    storedWishlist = JSON.parse(storedWishlistJSON)
    // console.log("Stored Wishlist found:", storedWishlist, storedWishlist.indexOf(1))
  }
  else {
    storedWishlist = []
  }

  var results = 0;
  var MovieElements = <tbody>
  {
    moviesList.map(function (name, index) {
      if (wishList.indexOf(name.id) == -1 && storedWishlist.indexOf(name.id) == -1) {
        // console.log(name.title, " is in the wishlist.")
        return
      }
      if (searchBarVal != ""){
        // console.log(typeof(name))
        if (name.title.includes && name.title.toLowerCase().includes(searchBarVal.toLowerCase())){
          results+=1;
          return <MovieElement
          title={name.title}
          genre={name.genre}
          year={name.year}
          key={name.id}
          setWishList={setWishList}
          prevWishList={wishList}
          index={name.id}
          in_wishlist={true}
        />
        }
        else if (name.genre.includes && name.genre.toLowerCase().includes(searchBarVal.toLowerCase())) {
          results+=1;
          return <MovieElement
          title={name.title}
          genre={name.genre}
          year={name.year}
          key={name.id}
          setWishList={setWishList}
          prevWishList={wishList}
          index={name.id}
          in_wishlist={true}
        />
        }
        else if (name.year.toString && name.year.toString().toLowerCase().includes(searchBarVal.toLowerCase())) {
          results+=1;
          return <MovieElement
          title={name.title}
          genre={name.genre}
          year={name.year}
          key={name.id}
          setWishList={setWishList}
          prevWishList={wishList}
          index={name.id}
          in_wishlist={true}
        />
        }
      }
      else {
        results+=1;
        return <MovieElement
          title={name.title}
          genre={name.genre}
          year={name.year}
          key={name.id}
          setWishList={setWishList}
          prevWishList={wishList}
          index={name.id}
          in_wishlist={true}
        />
      }
    })
  } 
  </tbody>

  var LoadingHeader;
  if (loading) {
    LoadingHeader = <h1>Loading Movies...</h1>
  }

  var NoResultsHeader
  if (results == 0 &&  !loading) {
    NoResultsHeader = <h1>No Results Found.</h1>
  }
  if (results == 0 && searchBarVal == "" && !loading) {
    NoResultsHeader = <h1>You did not add any movie to your Wishlist!</h1>
  }

  function handleSearchBarUpdate(event) {
    const text = event.target.value;
    setSearchBarVal(text)
  }

  return(
      <header className="HomePage-header">
          <Form.Control
            type="text"
            placeholder="Search Movies..."
            value={searchBarVal}
            onChange={handleSearchBarUpdate}
          />

          <Table hover>
          <thead>
              <tr>
              <th>Title</th>
              <th>Year</th>
              <th>Genre</th>
              <th>REMOVE from Wishlist</th>
              </tr>
          </thead>
          {MovieElements}
          </Table>
          {LoadingHeader}
          {NoResultsHeader}
      </header>
  )
}