import React, {useState} from "react";
import {Button} from 'react-bootstrap'
import heart from './heart.png';
import broken_heart from './broken-heart.png';

import { database } from './Firebase';
import { ref, child, get, set } from "firebase/database";

export default function MovieElement(props) {
    const movie_index = props.index
    const in_wishlist = props.in_wishlist

    var icon = heart;
    if (in_wishlist){
        icon = broken_heart;
    }

    function handleWishlistButton() {
        var wishListCopy = props.prevWishList.slice();

        if (in_wishlist) {
            const movieWishlistIndex = wishListCopy.indexOf(movie_index)
            if ( movieWishlistIndex != -1) {
                wishListCopy.splice(movieWishlistIndex, 1)
            }
            props.setWishList(wishListCopy)

            var prevStoredWishlist = localStorage.getItem("wishlist")
            if (prevStoredWishlist){
                prevStoredWishlist = JSON.parse(prevStoredWishlist)
                var newStoredWishlist = prevStoredWishlist.slice()
                
                const storedWishlistIndex = newStoredWishlist.indexOf(movie_index)
                console.log(newStoredWishlist, storedWishlistIndex, prevStoredWishlist)
                if (storedWishlistIndex != -1) {
                    newStoredWishlist.splice(storedWishlistIndex, 1)
                }
                localStorage.setItem("wishlist", JSON.stringify(newStoredWishlist))
                const userId = localStorage.getItem("userId");
                const db = database
                set(ref(db, `users/` + userId), JSON.stringify(newStoredWishlist));
            }
            else {
                localStorage.setItem("wishlist", JSON.stringify([]))
            }
        }
        else {
            wishListCopy.push(movie_index)
            props.setWishList(wishListCopy)

            var prevStoredWishlist = localStorage.getItem("wishlist")
            if (prevStoredWishlist){
                prevStoredWishlist = JSON.parse(prevStoredWishlist)
                var newStoredWishlist = prevStoredWishlist.slice()
                if (newStoredWishlist.indexOf(movie_index) != -1) {
                    // console.log(props.title, " already wishlisted.")
                    return;
                }
                newStoredWishlist.push(movie_index)
                localStorage.setItem("wishlist", JSON.stringify(newStoredWishlist))

                const userId = localStorage.getItem("userId");
                const db = database
                set(ref(db, `users/` + userId), JSON.stringify(newStoredWishlist));
            }
            else {
                localStorage.setItem("wishlist", JSON.stringify([movie_index]))
            }
        }
    }

    return(
        <tr>
            <td>{props.title}</td>
            <td>{props.year}</td>
            <td>{props.genre}</td>
            <td>
                <Button variant="secondary" size="lg" onClick={() => handleWishlistButton()}>
                    <img
                    alt=""
                    src={icon}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    />
                </Button>
            </td>
        </tr>
    )
}