import React from "react";
import { auth, provider } from './Firebase';
import { signInWithPopup } from "firebase/auth";
import logo from './popcorn.png';
import {Button} from 'react-bootstrap'

import { database } from './Firebase';
import { ref, child, get } from "firebase/database";

export default function LoginPage(props) {

    // console.log(props);

    function handleSignInWithGoogle() {
        signInWithPopup(auth, provider).then((result) => {
            const name = result.user.displayName;
            const userId = result.user.uid;
            localStorage.setItem("name", name);
            localStorage.setItem("userId", userId);
            localStorage.setItem("wishlist", JSON.stringify([]))

            get(child(ref(database), `users/` + userId)).then((snapshot) => {
                if (snapshot.exists()) {
                    console.log(snapshot.toJSON())
                    localStorage.setItem("wishlist", snapshot.toJSON())
                } else {
                  console.log("No data available");
                }
              }).catch((error) => {
                console.error(error);
            });

            props.setPageName("HomePage");
        }).catch((error) => {
            console.log(error);
        });
    }

    return(
        <div className="Login" style={{
            position: 'absolute', left: '50%', top: '20%', right: '10%', bottom: '90%',
            transform: 'translate(-50%, -50%)'
        }}>
            <header className="Login-header">
                <h1>Movies Room</h1>
                <img src={logo} className="App-logo" alt="logo" />
                <div>
                <Button variant="primary" size="lg" onClick={handleSignInWithGoogle}>
                    Sign In Using Google
                </Button>
                </div>
            </header>
        </div>
    )
}