// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
import { getDatabase } from "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyADOUsEUuPgjDtQZHj9p87vsWsG8eAvEhk",
  authDomain: "gleaming-entry-329819.firebaseapp.com",
  projectId: "gleaming-entry-329819",
  storageBucket: "gleaming-entry-329819.appspot.com",
  messagingSenderId: "32221264539",
  appId: "1:32221264539:web:76f7784de5c548ab52643e",
  measurementId: "G-H2C5QDVNPR",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const provider = new GoogleAuthProvider();
export const database = getDatabase(app, "https://gleaming-entry-329819-default-rtdb.europe-west1.firebasedatabase.app/");